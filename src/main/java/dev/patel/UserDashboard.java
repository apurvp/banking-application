package dev.patel;

import dev.patel.Utility.UserRegistration;
import dev.patel.Utility.userLogin;
import dev.patel.dao.AccountDaoImpl;
import dev.patel.model.Account;
import dev.patel.model.Customer;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.UUID;

public class UserDashboard {
    private ArrayList<Integer> accounts = new ArrayList<Integer>();
    public void CustomerMenu(Customer customer){
        Account act = new Account();
        AccountDaoImpl accountDao = new AccountDaoImpl();
        int userChoice = 0;
        Scanner scan = new Scanner(System.in);

        do{
            System.out.println("\n");
            System.out.println(" 1. Create a new Account");
            System.out.println(" 2. Check Balance");
            System.out.println(" 3. Deposit Money");
            System.out.println(" 4. Withdraw Money");
            System.out.println(" 5. Quit\n");
            System.out.println(" Enter your choice (e.g 2) :");

            try{
                userChoice = Integer.parseInt(scan.nextLine());
            }
            catch(NumberFormatException e){
                System.out.println("Invalid input.");
            }

            switch (userChoice){
                case 1:
                    act.setId(genrateAccountNumber());
                    System.out.println(" Account Number: " + act.getId());
                    double money = 0.0;
                    System.out.println(" Minimum Deposit $1:");
                    try{
                        money = Double.parseDouble(scan.nextLine());
                    }
                    catch(NumberFormatException e){
                        e.printStackTrace();
                    }
                    act.setBalance(money);
                    boolean addedAccount = accountDao.addNewAccount(act);
                    if(addedAccount){
                        System.out.println("You have created an account.");
                        System.out.println("Your account id is: "+act.getId());
                    }
                    break;
                case 2:
                    boolean balance = accountDao.checkBalance(act);
                    break;
                case 3:
                    boolean correct = false;
                    money = 0.0;
                    System.out.println("Enter amount you want to deposit.");
                    try{
                        money = Double.parseDouble(scan.nextLine());
                    }
                    catch(NumberFormatException e){
                        if(money<0.0){
                            e.printStackTrace();
                        }
                    }
                    boolean deposited = accountDao.deposit(act, money);
                    break;
                case 4:
                    break;
                case 5:
                    break;

            }
        }while(userChoice != 5);
    }
    public int genrateAccountNumber(){
        if(accounts.contains((Math.random()*100000))){
            int random = (int)(Math.random()*100000);
            accounts.add(random);
        }
        else accounts.add((int)(Math.random()*10));
        return accounts.get(accounts.size()-1);
    }
}
