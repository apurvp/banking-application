package dev.patel.model;

import java.util.List;

public class Customer {
    private int id;
    private String fullName;
    private String username;
    private String password;
    private List<Account> accountList;

    public Customer(){
        super();
    }


    public Customer(int id, String fullName, String username, String password, List<Account> accountList) {
        this.id = id;
        this.fullName = fullName;
        this.username = username;
        this.password = password;
        this.accountList = accountList;
    }

    public Customer(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    public void addACcount(Account act){
        this.accountList.add(act);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", accountList=" + accountList +
                '}';
    }
}



