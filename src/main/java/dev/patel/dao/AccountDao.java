package dev.patel.dao;

import dev.patel.Utility.ConnectionUtility;
import dev.patel.model.Account;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface AccountDao {
    public boolean addNewAccount(Account newAccount);
    public boolean checkBalance(Account account);
    public boolean deposit(Account account, double account_num);
}
