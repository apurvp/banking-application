package dev.patel.dao;

import dev.patel.Utility.ConnectionUtility;
import dev.patel.model.Account;
import dev.patel.model.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerDaoImpl implements CustomerDao{

    @Override
    public boolean userLogin(String username, String password){
        String sql = "select * from customer where username=? and p4ssword=?";
        try (Connection connection = ConnectionUtility.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);){
             ps.setString(1, username);
             ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                System.out.println("Logged In Successfully.");
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean addNewCustomer(Customer newCustomer){
        String sql = "insert into customer values (default, ?,?,?)";
        try (Connection connection = ConnectionUtility.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);){
                ps.setString(1,newCustomer.getFullName());
                ps.setString(2,newCustomer.getUsername());
                ps.setString(3,newCustomer.getPassword());
                int success = ps.executeUpdate();
                if(success>0){
                    return true;
                }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}
