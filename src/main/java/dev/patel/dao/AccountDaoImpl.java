package dev.patel.dao;

import dev.patel.Utility.ConnectionUtility;
import dev.patel.model.Account;
import dev.patel.model.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class AccountDaoImpl implements AccountDao{

    @Override
    public boolean addNewAccount(Account newAccount){
        String sql = "insert into account values (?,?)";
        try (Connection connection = ConnectionUtility.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);){
            ps.setInt(1,newAccount.getId());
            ps.setDouble(2,newAccount.getBalance());
            int success = ps.executeUpdate();
            if(success>0){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
    @Override
    public boolean checkBalance(Account account){
        String sql = "select * from account where id=? and balance=?";
        try (Connection connection = ConnectionUtility.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);){
            ps.setInt(1, account.getId());
            ps.setDouble(2, account.getBalance());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                System.out.println("Your Balance is:"+rs.getDouble(2));
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deposit(Account account, double amount) {
        amount+=account.getBalance();
        String sql = "select * from account where id=? and balance=?";
        try (Connection connection = ConnectionUtility.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setInt(1, account.getId());
            ps.setDouble(2, amount);
            int success = ps.executeUpdate();
            if (success > 0) {
                System.out.println("Balance Updated:");
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}
