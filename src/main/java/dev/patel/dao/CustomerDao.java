package dev.patel.dao;

import dev.patel.model.Customer;

public interface CustomerDao {
    public boolean userLogin(String username, String password);
    public boolean addNewCustomer(Customer newCustomer);
}
