package dev.patel.Utility;


import dev.patel.UserDashboard;
import dev.patel.dao.CustomerDaoImpl;
import dev.patel.model.Customer;

import java.util.Scanner;

public class userLogin {
    private Scanner scan = new Scanner(System.in);
    CustomerDaoImpl customerDao = new CustomerDaoImpl();

    public void userLogin(){
        do {
            System.out.println("Please enter your username.");
            String username = scan.nextLine();
            System.out.println("Please enter your password.");
            String password = scan.nextLine();
            boolean loogedIn = customerDao.userLogin(username, password);
            if(loogedIn) {
                Customer customer = new Customer(username, password);
                System.out.println("Welcome " + username);
                UserDashboard dash = new UserDashboard();
                dash.CustomerMenu(customer);
                break;
            }
            else
                System.out.println("Invalid username/password.");
        } while(true);
    }
}
