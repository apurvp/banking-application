package dev.patel.Utility;

import dev.patel.dao.AccountDao;
import dev.patel.dao.AccountDaoImpl;
import dev.patel.dao.CustomerDao;
import dev.patel.dao.CustomerDaoImpl;
import dev.patel.model.Account;
import dev.patel.model.Customer;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class UserRegistration {
    private int id=0;
    private ArrayList<Integer> accounts = new ArrayList<Integer>();
    CustomerDaoImpl customerDao = new CustomerDaoImpl();

    public void registerNewUser(){
        Scanner scan = new Scanner(System.in);
        do{
            Customer cust = new Customer();
            cust.setId(genrateCustomer_id());
            System.out.println("\n---------- New Customer Registration ----------\n");
            System.out.println(" Enter your Full Name:");
            cust.setFullName(scan.nextLine());
            System.out.println(" Enter your Username:");
            cust.setUsername(scan.nextLine());
            System.out.println(" Enter your Password:");
            cust.setPassword(scan.nextLine());
            System.out.println(" You can now login with your username and password.");
            cust.setAccountList(null);
            boolean addedCustomer = customerDao.addNewCustomer(cust);
            break;
        }while(true);
    }

    public int genrateCustomer_id(){
        if(accounts.contains((Math.random()*100000))){
            int random = (int)(Math.random()*100000);
            accounts.add(random);
        }
        else accounts.add((int)(Math.random()*10));
        return accounts.get(accounts.size()-1);
    }
}
