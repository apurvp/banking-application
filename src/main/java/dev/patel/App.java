package dev.patel;

import dev.patel.Utility.ConnectionUtility;
import dev.patel.Utility.UserRegistration;
import dev.patel.Utility.userLogin;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class App {
    public static void main(String[] args) {
        int userChoice = 0;
        Scanner scan = new Scanner(System.in);
        String u_id = UUID.randomUUID().toString();

        System.out.println(u_id);
        do{
            System.out.println("\n---------- Welcome to Revature Bank ----------\n");
            System.out.println(" 1. Customer Login");
            System.out.println(" 2. New Customer Registration");
            System.out.println(" 3. Quit\n");
            System.out.println(" Enter your choice (e.g 2) :");

            try{
                userChoice = Integer.parseInt(scan.nextLine());
            }
            catch(NumberFormatException e){
                System.out.println("Invalid Input");
            }
            switch (userChoice){
                case 1:
                    userLogin log = new userLogin();
                    log.userLogin();
                    break;
                case 2:
                    UserRegistration reg = new UserRegistration();
                    reg.registerNewUser();
                    break;
                case 3:
                    break;
            }
        }while(userChoice != 3);
    }
}
